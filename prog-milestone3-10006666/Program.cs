﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10006666
{
    public class Pizza
    {
        public static List<string> pizzaList = new List<string> { "Pepperoni", "Meatlovers", "Supreme", "Cheese", "Vegetarian" };

        public static List<string> order = new List<string> ();

        public static List<Tuple<string, double>> sizes = new List<Tuple<string, double>> ();

        public static List<Tuple<string, double>> drinks = new List<Tuple<string, double>> ();
    }
    class Program
    {
        static double sum = 0;

        static void size()
        {
            Dictionary<string, string> priceSize = new Dictionary<string, string>();
            priceSize.Add("Small", "$4.50");
            priceSize.Add("Medium", "$6.75");
            priceSize.Add("American", "$10.00");

            Console.WriteLine("Which size would you like?");
            foreach(var kvp in priceSize)
            {
                Console.WriteLine($"{kvp.Key} -- {kvp.Value}");
            }
            Console.WriteLine("(1/2/3):");
            var sizeNum = int.Parse(Console.ReadLine());

            switch(sizeNum)
            {
                case 1:    
                    Pizza.sizes.Add(Tuple.Create("Small", 4.50));
                    break;
                case 2:
                    Pizza.sizes.Add(Tuple.Create("Medium", 6.75));
                    break;
                case 3:
                    Pizza.sizes.Add(Tuple.Create("American", 10.00));
                    break;
                default:
                    Console.WriteLine("Please make a selection.");
                    break;
            }
        }

        static void pizza()
        {
            var ansPiz = "";
            do
            {
                Console.WriteLine("Please enter which pizza you would like.");
                Console.WriteLine();
                Console.WriteLine("-- Menu --");
                Pizza.pizzaList.ForEach(Console.WriteLine);
                Console.WriteLine();
                Console.WriteLine("(1/2/3/4/5): ");
                var menuNum = int.Parse(Console.ReadLine());
                Console.Clear();

                switch (menuNum)
                {
                    case 1:
                        size();
                        Pizza.order.Add(Pizza.pizzaList[0]);
                        break;
                    case 2:
                        size();
                        Pizza.order.Add(Pizza.pizzaList[1]);
                        break;
                    case 3:
                        size();
                        Pizza.order.Add(Pizza.pizzaList[2]);
                        break;
                    case 4:
                        size();
                        Pizza.order.Add(Pizza.pizzaList[3]);
                        break;
                    case 5:
                        size();
                        Pizza.order.Add(Pizza.pizzaList[4]);
                        break;
                    default:
                        Console.WriteLine("Please select a pizza.");
                        break;

                }
                Console.WriteLine("Would you like to purchase more pizza? (y/n): ");
                ansPiz = Console.ReadLine();
                Console.Clear();
            } while (ansPiz == "y" || ansPiz == "Y");
        }

        static void drink()
        {
            Console.WriteLine("Please select one.");
            var drink = new Dictionary<string, string>();
            drink.Add("Coke", "$2.50");
            drink.Add("Sprite", "$2.50");
            drink.Add("Dr. Pepper (the superior drink)", "$2.00");

            foreach (var d in drink)
            {
                Console.WriteLine($"{d.Key} -- {d.Value}");
            }
            Console.WriteLine("(1/2/3): ");
            var ansDrink = int.Parse(Console.ReadLine());

            switch(ansDrink)
            {
                case 1:
                    Pizza.drinks.Add(Tuple.Create("Coke", 2.50));
                    break;
                case 2:
                    Pizza.drinks.Add(Tuple.Create("Sprite", 2.50));
                    break;
                case 3:
                    Pizza.drinks.Add(Tuple.Create("Dr. Pepper", 2.00));
                    break;
                default:
                    Console.WriteLine("Please make a selection.");
                    break;
            }

        }

        static void total()
        {
            Console.WriteLine("Your current order is: ");

            foreach (var i in Pizza.order.Zip(Pizza.sizes, (o, s) => new { o, s }))
            {
                Console.WriteLine(i.o + "--" + i.s);
            }
            foreach (var d in Pizza.drinks)
            {
                Console.WriteLine($"{d.Item1} -- ({d.Item2})");    
            }
            Console.WriteLine();
            
            foreach (var i2 in Pizza.sizes)
            {
                sum += i2.Item2;
            }
            foreach (var i3 in Pizza.drinks)
            {
                sum += i3.Item2;
            }
            Console.WriteLine($"Current total is: ${sum}");
        }

        static void payment()
        {
            Console.WriteLine("Please enter the amount you wish to pay: ");
            var pay = double.Parse(Console.ReadLine());
            
            if (pay < sum)
            {
                Console.WriteLine("This is not enough, please enter more: ");
                var payMo = double.Parse(Console.ReadLine());

                pay = pay + payMo;

                if (pay < sum)
                {
                    Console.WriteLine("This is still not enough. Come on: ");
                    var payMo2 = double.Parse(Console.ReadLine());

                    pay = pay + payMo2;

                    if (pay < sum)
                    {
                        Console.WriteLine("Please leave.");
                    }
                    else if (pay == sum)
                    {
                        Console.WriteLine("Thank you for your patronage.");
                    }
                    if (pay > sum)
                    {
                        double change = pay - sum;
                        Console.WriteLine($"Thank you. Your change is: ${change}");
                    }
                }
                if (pay > sum)
                {
                    double change = pay - sum;
                    Console.WriteLine($"Thank you. Your change is: ${change}");
                }
            }
            else if (pay > sum)
            {
                double change = pay - sum;
                Console.WriteLine($"Thank you. Your change is: ${change}");
            }
            else if (pay == sum)
            {
                Console.WriteLine("Thank you for your patronage.");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome, Please enter your first name: ");
            var name = Console.ReadLine();
            Console.WriteLine("Please enter your phone number: ");
            var num = Console.ReadLine();
            Console.Clear();

            pizza();

            Console.WriteLine("Would you like to order drinks also? (y/n): ");
            var ansDrink = Console.ReadLine();
            if (ansDrink == "y" || ansDrink == "Y")
            {
                Console.Clear();
                drink();
            }
            Console.Clear();

            total();
            payment();
        }
    }
}
